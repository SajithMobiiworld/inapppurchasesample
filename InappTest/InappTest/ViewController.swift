//
//  ViewController.swift
//  InappTest
//
//  Created by Sajith P on 09/05/18.
//  Copyright © 2018 Sajith P. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit

let nonConsumableProdectId = "com.mobiiworld.inapptesttt.firstproduct"
let consumableProdectId = "com.mobiiworld.inapptesttt.secondproduct"

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var nonConsumableBuyButton: UIButton!
    @IBOutlet weak var nonConsumableRestoreButton: UIButton!
    @IBOutlet weak var purchasedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.verifyPurchase(nonConsumableProdectId)
    }

    // MARK:- Action Methods
    @IBAction func consumableInappInfo(_ sender: Any) {
        self.getInfo(consumableProdectId)
//        self.verifyPurchase(consumableProdectId)
    }
    
    @IBAction func nonConsumableInappInfo(_ sender: Any) {
        self.getInfo(nonConsumableProdectId)
//        self.verifyPurchase(nonConsumableProdectId)
    }
    
    @IBAction func consumableInappBuy(_ sender: Any) {
        self.purchase(consumableProdectId)
    }
    
    @IBAction func nonConsumableInappBuy(_ sender: Any) {
        self.purchase(nonConsumableProdectId)
    }
    
    @IBAction func restoreAll(_ sender: Any) {
        self.activityIndicator.startAnimating()
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            self.activityIndicator.stopAnimating()

            for purchase in results.restoredPurchases {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                } else if purchase.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
            self.verifyPurchase(nonConsumableProdectId)
            self.showAlert(self.alertForRestorePurchases(results))
        }
    }
    
    // MARK:- Private Methods
    func purchase(_ productId: String) {
        self.activityIndicator.startAnimating()
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            self.activityIndicator.stopAnimating()
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                self.setleInappPurchased(productId: productId)
            }
            if let alert = self.alertForPurchaseResult(result) {
                self.showAlert(alert)
            }
        }
    }
    
    func getInfo(_ productId: String) {
        
        self.activityIndicator.startAnimating()
        SwiftyStoreKit.retrieveProductsInfo([productId]) { result in
            self.activityIndicator.stopAnimating()
            self.showAlert(self.alertForProductRetrievalInfo(result))
        }
    }
    
    func verifyPurchase(_ productId: String) {
        
        let appleValidator = AppleReceiptValidator(service: .sandbox, sharedSecret: "4a6f17d32d1c4849bc055a5bec895d03")
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                let purchaseResult = SwiftyStoreKit.verifyPurchase(
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased://(let receiptItem):
                    self.setleInappPurchased(productId: productId)
                case .notPurchased:
                    print("The user has never purchased \(productId)")
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
            }
        }
    }
   
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }

    
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            return nil
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
            }
        }
    }
    
    func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
        
        if results.restoreFailedPurchases.count > 0 {
            print("Restore Failed: \(results.restoreFailedPurchases)")
            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
        } else if results.restoredPurchases.count > 0 {
            print("Restore Success: \(results.restoredPurchases)")
            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
        } else {
            print("Nothing to Restore")
            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let invalidProductId = result.invalidProductIDs.first {
            return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
        } else {
            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
            return alertWithTitle("Could not retrieve product info", message: errorString)
        }
    }
    
    func setleInappPurchased(productId: String) {
        if productId == nonConsumableProdectId {
            self.nonConsumableBuyButton.isHidden = true
            self.nonConsumableRestoreButton.isHidden = true
            self.purchasedLabel.isHidden = false
        }
    }
}

